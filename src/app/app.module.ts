import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseComponent } from './components/dashboard/base-parts/base/base.component';
import { LoginComponent } from './components/auth/login/login.component';
import { SignupComponent } from './components/auth/signup/signup.component';
import { HeaderComponent } from './components/dashboard/base-parts/header/header.component';
import { SideMenuComponent } from './components/dashboard/base-parts/side-menu/side-menu.component';
import { SideSettingMenuComponent } from './components/dashboard/base-parts/side-setting-menu/side-setting-menu.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { ForgotPassComponent } from './components/auth/forgot-pass/forgot-pass.component';
import { MainComponent } from './components/dashboard/user/main/main.component';
import { AllCourcesComponent } from './components/dashboard/cources/all-cources/all-cources.component';
import { MyCourcesComponent } from './components/dashboard/cources/my-cources/my-cources.component';
import { DegreesComponent } from './components/dashboard/cources/degrees/degrees.component';
import { QuestionsComponent } from './components/dashboard/cources/questions/questions.component';
import { AskQuestionComponent } from './components/dashboard/cources/ask-question/ask-question.component';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    LoginComponent,
    SignupComponent,
    HeaderComponent,
    SideMenuComponent,
    SideSettingMenuComponent,
    ProfileComponent,
    ForgotPassComponent,
    MainComponent,
    AllCourcesComponent,
    MyCourcesComponent,
    DegreesComponent,
    QuestionsComponent,
    AskQuestionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
