import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-setting-menu',
  templateUrl: './side-setting-menu.component.html',
  styleUrls: ['./side-setting-menu.component.css']
})
export class SideSettingMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  lightSidebarMenu() {
    $("body").removeClass("menu_dark logo-black");
    $("body").addClass("menu_light logo-white");
    var menu_option = "menu_light";
    localStorage.setItem("demo2_choose_logoheader", "logo-white");
    localStorage.setItem("demo2_menu_option", menu_option);
  }

  darkSidebarMenu() {
    $("body").removeClass("menu_light logo-white");
    $("body").addClass("menu_dark logo-black");
    var menu_option = "menu_dark";
    localStorage.setItem("demo2_choose_logoheader", "logo-black");
    localStorage.setItem("demo2_menu_option", menu_option);
  }

  lightTheme() {
    $("body").removeClass("dark submenu-closed menu_dark logo-black");
    $("body").addClass("light submenu-closed menu_light logo-white");
    var theme = "light";
    var menu_option = "menu_light";
    localStorage.setItem("demo2_choose_logoheader", "logo-white");
    localStorage.setItem("demo2_choose_skin", "theme-black");
    localStorage.setItem("demo2_theme", theme);
    localStorage.setItem("demo2_menu_option", menu_option);
  }

  darkTheme() {
    $("body").removeClass("light submenu-closed menu_light logo-white");
    $("body").addClass("dark submenu-closed menu_dark logo-black");

    var theme = "dark";
    var menu_option = "menu_dark";
    localStorage.setItem("demo2_choose_logoheader", "logo-black");
    localStorage.setItem("demo2_choose_skin", "theme-black");
    localStorage.setItem("demo2_theme", theme);
    localStorage.setItem("demo2_menu_option", menu_option);
  }

}
