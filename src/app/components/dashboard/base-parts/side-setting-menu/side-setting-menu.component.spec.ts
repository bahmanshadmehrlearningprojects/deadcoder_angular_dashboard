import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideSettingMenuComponent } from './side-setting-menu.component';

describe('SideSettingMenuComponent', () => {
  let component: SideSettingMenuComponent;
  let fixture: ComponentFixture<SideSettingMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideSettingMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideSettingMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
