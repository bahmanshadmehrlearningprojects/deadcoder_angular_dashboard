import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    //set theme on startup
    if (localStorage.getItem("demo2_theme")) {
      $("body").removeClass("dark light");
      jQuery("body").addClass(localStorage.getItem("demo2_theme"));
    }


    // set dark sidebar menu on startup
    if (localStorage.getItem("demo2_menu_option")) {
      jQuery("body").addClass(localStorage.getItem("demo2_menu_option"));
    } else {
      jQuery("body").addClass("menu_dark");
    }

    // set header color on startup
    if (localStorage.getItem("demo2_choose_skin")) {
      jQuery("body").addClass(localStorage.getItem("demo2_choose_skin"));
    } else {
      jQuery("body").addClass("theme-black");
    }
    if (localStorage.getItem("demo2_choose_skin_active")) {
      $(".right-sidebar .demo-choose-skin li").each(function (index) {
        jQuery(this).removeClass("actived");
        if (jQuery(this).attr('data-theme') == localStorage.getItem("demo2_choose_skin_active")) {
          jQuery(this).addClass("actived");
        }
      });
    }
    // set logo color on startup
    if (localStorage.getItem("demo2_choose_logoheader")) {
      jQuery("body").addClass(localStorage.getItem("demo2_choose_logoheader"));
    } else {
      jQuery("body").addClass("logo-black");
    }
    if (localStorage.getItem("demo2_choose_logoheader_active")) {
      $(".right-sidebar .demo-choose-logoheader li").each(function (index) {
        jQuery(this).removeClass("actived");
        if (jQuery(this).attr('data-theme') == localStorage.getItem("demo2_choose_logoheader_active")) {
          jQuery(this).addClass("actived");
        }
      });
    }
  }

}
