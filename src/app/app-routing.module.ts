import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseComponent } from './components/dashboard/base-parts/base/base.component';
import { LoginComponent } from './components/auth/login/login.component';
import { SignupComponent } from './components/auth/signup/signup.component';
import { ProfileComponent } from './components/user/profile/profile.component';
import { ForgotPassComponent } from './components/auth/forgot-pass/forgot-pass.component';
import { MainComponent } from './components/dashboard/user/main/main.component';
import { AllCourcesComponent } from './components/dashboard/cources/all-cources/all-cources.component';
import { MyCourcesComponent } from './components/dashboard/cources/my-cources/my-cources.component';
import { DegreesComponent } from './components/dashboard/cources/degrees/degrees.component';
import { QuestionsComponent } from './components/dashboard/cources/questions/questions.component';
import { AskQuestionComponent } from './components/dashboard/cources/ask-question/ask-question.component';

const routes: Routes = [
  {
    path: 'dashboard', component: BaseComponent, children: [
      { path: '', component: MainComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'all_cources', component: AllCourcesComponent },
      { path: 'my_cources', component: MyCourcesComponent },
      { path: 'certificates', component: DegreesComponent },
      { path: 'questions', component: QuestionsComponent },
      { path: 'new_question', component: AskQuestionComponent },
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'reset_password', component: ForgotPassComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
